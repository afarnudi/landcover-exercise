def is_palindrome(string):
    string = string.lower()
    for char in [" ", "?", "!", "-", "+"]:
        string = string.replace(char, "")
    return string == string[::-1]
